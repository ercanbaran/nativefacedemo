/* globals */
const Page       = require("nf-core/ui/page");
const extend     = require("js-base/core/extend");
const FlexLayout = require('nf-core/ui/flexlayout');
const Color      = require('nf-core/ui/color');
const Button     = require('nf-core/ui/button');
const Animator   = require('nf-core/ui/animator');
const ImageView  = require('nf-core/ui/imageview');
const Image		 = require('nf-core/ui/image');
const ImageFillType = require('nf-core/ui/imagefilltype');
const Screen		= require('nf-core/device/screen');
const View		= require('nf-core/ui/view');
var clicked = false;

var pgTest = extend(Page)(
    function(_super,params)
    {
    	var self = this;
        _super(this, 
        {
		    onShow : function(){
		    	this.headerBar.visible = false;
		        this.statusBar.visible = false;
		        
		        // if(global.Pages)
		        // {
		        //   global.Pages.sliderDrawer.enabled = false
		
		        // }
		        
		    var logoImage = new ImageView();
        	logoImage.image = Image.createFromFile("images://logo.png");
        	logoImage.width = 0;
        	logoImage.height = 0;
        	logoImage.imageFillType = ImageFillType.ASPECTFIT;
        	logoImage.positionType = FlexLayout.PositionType.ABSOLUTE;
        	this.layout.addChild(logoImage);
        	
        	var myContainer = new View();
        	myContainer.width = 200
        	myContainer.height = 300
        	myContainer.backgroundColor = Color.BLUE
        	myContainer.left = 100
        	myContainer.top = Screen.height;
        	myContainer.positionType = FlexLayout.PositionType.ABSOLUTE;
        	this.layout.addChild(myContainer);
        	this.layout.applyLayout()



        	Animator.animate(self.layout, 500, function() {
		                    logoImage.top = Number.NaN;
		                    logoImage.right = Number.NaN
		                    logoImage.left = 1;
		                    logoImage.bottom = 1;
		                    logoImage.width = 25;
		                    logoImage.height = 25;
		                    console.log("1")
		                }).then(500, function() {
		                	console.log("2")

		                    logoImage.top = Number.NaN;
		                    logoImage.left = Number.NaN;
		                    logoImage.right = 1;
		                    logoImage.bottom = 1;
		                    logoImage.width = 50;
		                    logoImage.height = 50;
		                }).then(500, function() {
		                	console.log("3")
		                    logoImage.top = Number.NaN;
		                    logoImage.left = Number.NaN;
		                    logoImage.right = 1;
		                    logoImage.bottom = 1;
		                    logoImage.width = 50;
		                    logoImage.height = 50;
		                    
		                }).then(500, function() {
		                	console.log("4")
		                	logoImage.left = Number.NaN;
		                	logoImage.bottom = Number.NaN;
		                    logoImage.top = 1;
		                    logoImage.right = 1;
		                    logoImage.width = 75;
		                    logoImage.height = 75;
		                }).then(500, function() {
		                	logoImage.bottom = Number.NaN;
		                    logoImage.right = Number.NaN;
		                    logoImage.top = 100;
		                    logoImage.left = 150;
		                    
		                    logoImage.width = 100;
		                    logoImage.height = 100;
		                }).then(1000, function() {
		                	myContainer.top = 200;
		                }).complete(function() {
		                    //alert("Animation complete.");
		                });
        	// logoImage.touchEnabled = true;
        	// logoImage.onTouch = function()
        	// {
        	// 	if (!clicked)
        		
		       //     clicked = true    
        	// }
		    },
		    onLoad : function(){
		    
		    this.layout.backgroundColor = Color.createGradient({startColor: Color.create("#9D1B55"), endColor:Color.create("#D9595B"), direction: Color.GradientDirection.DIAGONAL_LEFT});
		   
        	
        	
        	
        	
		    	
 		    
		    }
        });
	}
);  
module.exports = pgTest;