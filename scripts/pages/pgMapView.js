/* globals */
const Page              = require("nf-core/ui/page");
const extend            = require("js-base/core/extend");
const componentStyler   = require("js-base/core/styler").componentStyler();
const FlexLayout        = require('nf-core/ui/flexlayout');
const AbsoluteLayout    = require('nf-core/ui/absolutelayout');
const Color             = require('nf-core/ui/color');
const ImageView         = require('nf-core/ui/imageview');
const Image             = require('nf-core/ui/image');
const Button            = require('nf-core/ui/button');
const Font              = require('nf-core/ui/font');
const Label             = require("nf-core/ui/label");
const HeaderBarItem     = require('nf-core/ui/headerbaritem');
const AlertView         = require('nf-core/ui/alertview');
const StatusBarStyle    = require("nf-core/ui/statusbarstyle");
const MapView           = require('nf-core/ui/mapview');
const MapViewType       = require('nf-core/ui/mapview/maptype');
const Menu              = require('nf-core/ui/menu');
const MenuItem          = require('nf-core/ui/menuitem');
const Router            = require('nf-core/ui/router');
const PageConstants     = require('pages/PageConstants');

var pgMapView = extend(Page)(
    function(_super,params)
    {
    	var self = this;
        _super(this, 
        {
		    onShow : function(){
		    	self.headerBar.title = lang['pgMapView.title'];
            
                self.headerBar.displayShowHomeEnabled = true;
                self.headerBar.android.displayShowHomeEnabled = true;
                self.statusBar.ios.style = StatusBarStyle.LIGHTCONTENT;
		    },
		    onLoad : function(){
		    self.headerBar.visible = true;
           self.headerBar.backgroundColor = Color.create("#9D1B55");
            self.headerBar.titleColor = Color.WHITE;
            self.headerBar.itemColor = Color.WHITE;

        self.layout.backgroundColor = Color.createGradient({startColor: Color.create("#9D1B55"), endColor:Color.create("#D9595B"), direction: Color.GradientDirection.DIAGONAL_LEFT});


        var menu = new Menu();
            
        menu.headerTitle = "Change Map Type";
        
        var menuItem1    = new MenuItem({title : "Normal"});
        var menuItem2   = new MenuItem({title : "Hybrid"});
        var menuItem3   = new MenuItem({title : "Satellite"});
        
        menuItem1.onSelected = function(){
            mapview.type = MapViewType.NORMAL
        }
        
        menuItem2.onSelected = function(){
            mapview.type = MapViewType.HYBRID
        }
        
        menuItem3.onSelected = function(){
            mapview.type = MapViewType.SATELLITE
        }
        menu.items = [menuItem1,menuItem2,menuItem3];
        
        var addItem = new HeaderBarItem({
            title: "Type",
            color : Color.WHITE,
            onPress: function() {
                menu.show(self);
            }
        });
        self.headerBar.setItems([addItem]);
        
        var mapview = new MapView();
        
            mapview.flexGrow = 1
            mapview.backgroundColor = Color.RED
            mapview.alignSelf = FlexLayout.AlignSelf.STRETCH
            
            
            mapview.onCreate = function(){
                    mapview.type = MapViewType.NORMAL;//0 normal //1 satelite //2 hybrid
                mapview.zoomEnabled = true;
                mapview.rotateEnabled = true;
                mapview.scrollEnabled = true;
                mapview.compassEnabled = true;
                mapview.centerLocation = {
                   latitude: 40.92149,
                   longitude: 29.154512
                };
            
                var myPin = new MapView.Pin({
                title : "Donerci Ali Usta",
                location : {latitude: 40.92149,longitude: 29.154512},
                subtitle : "Merkez Sube",
                color : Color.BLUE
            });
            
            mapview.addPin(myPin);

            };
            
            self.layout.addChild(mapview);
		    }
        });
	}
);  



module.exports = pgMapView;
