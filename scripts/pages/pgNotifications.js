/* globals */
const Page       = require("nf-core/ui/page");
const extend     = require("js-base/core/extend");
const FlexLayout = require('nf-core/ui/flexlayout');
const Color      = require('nf-core/ui/color');
const Button     = require('nf-core/ui/button');
const Router            = require('nf-core/ui/router');
const PageConstants     = require('pages/PageConstants');

var pgNotifications = new Page({
    onShow : function()
        {
            pgNotifications.headerBar.visible = true;
            pgNotifications.headerBar.title = lang['pgNotifications.title'];
            pgNotifications.headerBar.backgroundColor = Color.create("#9D1B55");
            pgNotifications.headerBar.titleColor = Color.WHITE;
            pgNotifications.headerBar.itemColor = Color.WHITE;
            pgNotifications.headerBar.displayShowHomeEnabled = true;
        },
    onLoad : function(){
            
        
    }
    
});    

module.exports = pgNotifications;