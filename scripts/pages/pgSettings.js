/* globals */
const Page              = require("nf-core/ui/page");
const extend            = require("js-base/core/extend");
const componentStyler   = require("js-base/core/styler").componentStyler();
const FlexLayout        = require('nf-core/ui/flexlayout');
const AbsoluteLayout    = require('nf-core/ui/absolutelayout');
const Color             = require('nf-core/ui/color');
const ImageView         = require('nf-core/ui/imageview');
const Image             = require('nf-core/ui/image');
const Button            = require('nf-core/ui/button');
const Font              = require('nf-core/ui/font');
const Label             = require("nf-core/ui/label");
const ListView          = require('nf-core/ui/listview');
const ListViewItem      = require('nf-core/ui/listviewitem');
const TextAlignment     = require('nf-core/ui/textalignment');
const HeaderBarItem     = require('nf-core/ui/headerbaritem');
const AlertView         = require('nf-core/ui/alertview');
const Slider            = require('nf-core/ui/slider');
const Switch            = require('nf-core/ui/switch');
const System            = require('nf-core/device/system');
const Hardware          = require('nf-core/device/hardware');
const StatusBarStyle    = require("nf-core/ui/statusbarstyle");
const ScrollView        = require('nf-core/ui/scrollview');
const Router            = require('nf-core/ui/router');
const PageConstants     = require('pages/PageConstants');


var pgSettings = new extend(Page)(
    function(_super,params)
    {
        _super(this,{
    onShow : function()
        {
            this.headerBar.title = lang['pgSettings.title'];
            
            this.headerBar.displayShowHomeEnabled = true;
            this.headerBar.android.displayShowHomeEnabled = true;
            this.statusBar.ios.style = StatusBarStyle.LIGHTCONTENT;

        },
    onLoad : function(){
        this.headerBar.visible = true;
        this.headerBar.titleColor = Color.WHITE;
        this.headerBar.itemColor = Color.WHITE;
                    this.headerBar.backgroundColor = Color.create("#9D1B55");

        this.layout.backgroundColor = Color.createGradient({startColor: Color.create("#9D1B55"), endColor:Color.create("#D9595B"), direction: Color.GradientDirection.DIAGONAL_LEFT});

        var menuIcon = new HeaderBarItem({
            color : Color.WHITE,
            image: Image.createFromFile("images://Menuiconn.png"),
            onPress: function() {
                Router.sliderDrawer.show();
            }
        });
        this.headerBar.setLeftItem(menuIcon);
        
        

        
        var container = new FlexLayout();
        container.flexDirection = FlexLayout.FlexDirection.COLUMN;
        container.alignItems = FlexLayout.AlignItems.STRETCH;
        container.backgroundColor = Color.WHITE;
        container.borderWidth = 1;
        container.borderColor = Color.TRANSPARENT;
        container.borderRadius = 10;
        container.top = 20;
        container.bottom = 20;
        container.right = 20;
        container.left = 20;
        container.positionType = FlexLayout.PositionType.ABSOLUTE;
        
        var myScrollView = new ScrollView({
            flexGrow : 1,
            alignSelf: FlexLayout.AlignSelf.STRETCH,
            //backgroundColor : Color.YELLOW
        });
        
        var contentContainer = new FlexLayout({
            flexGrow : 1,
            //backgroundColor : Color.GREEN,
            height : 800
        })
        
        myScrollView.addChild(contentContainer);
        container.addChild(myScrollView);

       // myScrollView.align = ScrollView.Align.VERTICAL;
        
        var titleLabel = new Label();
        titleLabel.font =  Font.create("Lato",20,Font.BOLD);
        titleLabel.backgroundColor = Color.TRANSPARENT;
        titleLabel.textColor = Color.BLACK;
        titleLabel.textAlignment = TextAlignment.MIDCENTER;
        titleLabel.text = lang['pgSettings.subtitle'];
        titleLabel.marginTop = 10;
        contentContainer.addChild(titleLabel)
        addDivider(contentContainer)
        
        
        var subContainer1 = new FlexLayout();
        subContainer1.flexDirection = FlexLayout.FlexDirection.ROW;
        subContainer1.alignItems = FlexLayout.AlignItems.STRETCH;
        subContainer1.height = 60;
        
        var titleBrightness = new Label();
        titleBrightness.font =  Font.create("Lato",16,Font.NORMAL);
        titleBrightness.backgroundColor = Color.TRANSPARENT;
        titleBrightness.textColor = Color.BLACK;
        titleBrightness.textAlignment = TextAlignment.MIDLEFT;
        titleBrightness.text = lang['pgSettings.brightness'];
        titleBrightness.margin = 10;
        titleBrightness.flexGrow = 1;
        subContainer1.addChild(titleBrightness);
        
        var brightnessSlider = new Slider({
            width: 150,
            maxValue: 100,
            minValue: 0,
            value: 40,
            flexDirection:1,
            margin:10,
            minTrackColor: Color.create("#FFA200"),
            maxTrackColor: Color.create("#10000000"),
            thumbColor: Color.create("#CCCCCC"),
            onValueChange: function() {
                //console.log("Slider's value: " + mySlider.value);
            }
        });
        subContainer1.addChild(brightnessSlider);
        contentContainer.addChild(subContainer1);
        addDivider(contentContainer,20)
        
        var subContainer2 = new FlexLayout();
        subContainer2.flexDirection = FlexLayout.FlexDirection.ROW;
        subContainer2.alignItems = FlexLayout.AlignItems.STRETCH;
        subContainer2.height = 60;
        
        var titleSwitch = new Label();
        titleSwitch.font =  Font.create("Lato",16,Font.NORMAL);
        titleSwitch.backgroundColor = Color.TRANSPARENT;
        titleSwitch.textColor = Color.BLACK;
        titleSwitch.textAlignment = TextAlignment.MIDLEFT;
        titleSwitch.text = lang['pgSettings.vibrate'];
        titleSwitch.margin = 10;
        titleSwitch.flexGrow = 1;
        subContainer2.addChild(titleSwitch);
        
        var mySwitch = new Switch({
            margin:10,
            alignSelf : FlexLayout.AlignSelf.CENTER,
            onToggleChanged : function()
            {
                if(mySwitch.toggle)
                {
                    System.vibrate(1000);
                }
            }
        });
        subContainer2.addChild(mySwitch);
        contentContainer.addChild(subContainer2);
        addDivider(contentContainer,20)
        
        
        var subContainer3 = new FlexLayout();
        subContainer3.flexDirection = FlexLayout.FlexDirection.ROW;
        subContainer3.alignItems = FlexLayout.AlignItems.STRETCH;
        subContainer3.height = 60;
        
        var titleBrandModel = new Label();
        titleBrandModel.font =  Font.create("Lato",16,Font.NORMAL);
        titleBrandModel.backgroundColor = Color.TRANSPARENT;
        titleBrandModel.textColor = Color.BLACK;
        titleBrandModel.textAlignment = TextAlignment.MIDLEFT;
        titleBrandModel.text = lang['pgSettings.brandmodel'];
        titleBrandModel.margin = 10;
        titleBrandModel.flexGrow = 1;
        subContainer3.addChild(titleBrandModel);
        
        var valueBrandModel = new Label();
        valueBrandModel.font =  Font.create("Lato",14,Font.NORMAL);
        valueBrandModel.backgroundColor = Color.TRANSPARENT;
        valueBrandModel.textColor = Color.create("#50000000");
        valueBrandModel.text = Hardware.brandModel;
        valueBrandModel.margin = 15;
        subContainer3.addChild(valueBrandModel);
        
        contentContainer.addChild(subContainer3);
        addDivider(contentContainer,20);
        
        var subContainer4 = new FlexLayout();
        subContainer4.flexDirection = FlexLayout.FlexDirection.ROW;
        subContainer4.alignItems = FlexLayout.AlignItems.STRETCH;
        subContainer4.height = 60;
        
        var titleBrandName = new Label();
        titleBrandName.font =  Font.create("Lato",16,Font.NORMAL);
        titleBrandName.backgroundColor = Color.TRANSPARENT;
        titleBrandName.textColor = Color.BLACK;
        titleBrandName.textAlignment = TextAlignment.MIDLEFT;
        titleBrandName.text = lang['pgSettings.brandname'];
        titleBrandName.margin = 10;
        titleBrandName.flexGrow = 1;
        subContainer4.addChild(titleBrandName);
        
        var valueBrandName = new Label();
        valueBrandName.font =  Font.create("Lato",14,Font.NORMAL);
        valueBrandName.backgroundColor = Color.TRANSPARENT;
        valueBrandName.textColor = Color.create("#50000000");
        valueBrandName.text = Hardware.brandName
        valueBrandName.margin = 15;
        subContainer4.addChild(valueBrandName);
        
        contentContainer.addChild(subContainer4);
        addDivider(contentContainer,20)
        
        var subContainer5 = new FlexLayout();
        subContainer5.flexDirection = FlexLayout.FlexDirection.ROW;
        subContainer5.alignItems = FlexLayout.AlignItems.STRETCH;
        subContainer5.height = 60;
        
        var titleIMEI = new Label();
        titleIMEI.font =  Font.create("Lato",16,Font.NORMAL);
        titleIMEI.backgroundColor = Color.TRANSPARENT;
        titleIMEI.textColor = Color.BLACK;
        titleIMEI.textAlignment = TextAlignment.MIDLEFT;
        titleIMEI.text = lang['pgSettings.imei'];
        titleIMEI.margin = 10;
        titleIMEI.flexGrow = 1;
        subContainer5.addChild(titleIMEI);
        
        var valueIMEI = new Label();
        valueIMEI.font =  Font.create("Lato",14,Font.NORMAL);
        valueIMEI.backgroundColor = Color.TRANSPARENT;
        valueIMEI.textColor = Color.create("#50000000");
        //valueIMEI.text = Hardware.IMEI
        valueIMEI.margin = 15;
        subContainer5.addChild(valueIMEI);
        
        contentContainer.addChild(subContainer5);
        addDivider(contentContainer,20);
        
        var subContainer6 = new FlexLayout();
        subContainer6.flexDirection = FlexLayout.FlexDirection.ROW;
        subContainer6.alignItems = FlexLayout.AlignItems.STRETCH;
        subContainer6.height = 60;
        
        var titleUID = new Label();
        titleUID.font =  Font.create("Lato",16,Font.NORMAL);
        titleUID.backgroundColor = Color.TRANSPARENT;
        titleUID.textColor = Color.BLACK;
        titleUID.textAlignment = TextAlignment.MIDLEFT;
        titleUID.text = lang['pgSettings.uid'];
        titleUID.margin = 10;
        titleUID.flexGrow = 1;
        subContainer6.addChild(titleUID);
        
        var valueUID = new Label();
        valueUID.font =  Font.create("Lato",12,Font.NORMAL);
        valueUID.backgroundColor = Color.TRANSPARENT;
        valueUID.textColor = Color.create("#50000000");
        valueUID.text = Hardware.UID
        valueUID.margin = 15;
        subContainer6.addChild(valueUID);
        
        contentContainer.addChild(subContainer6);
        addDivider(contentContainer,20)
        
        
        var subContainer7 = new FlexLayout();
        subContainer7.flexDirection = FlexLayout.FlexDirection.ROW;
        subContainer7.alignItems = FlexLayout.AlignItems.STRETCH;
        subContainer7.height = 60;
        
        var titleUID2 = new Label();
        titleUID2.font =  Font.create("Lato",16,Font.NORMAL);
        titleUID2.backgroundColor = Color.TRANSPARENT;
        titleUID2.textColor = Color.BLACK;
        titleUID2.textAlignment = TextAlignment.MIDLEFT;
        titleUID2.text = lang['pgSettings.uid'];
        titleUID2.margin = 10;
        titleUID2.flexGrow = 1;
        subContainer7.addChild(titleUID2);
        
        var valueUID2 = new Label();
        valueUID2.font =  Font.create("Lato",12,Font.NORMAL);
        valueUID2.backgroundColor = Color.TRANSPARENT;
        valueUID2.textColor = Color.create("#50000000");
        valueUID2.text = Hardware.UID
        valueUID2.margin = 15;
        subContainer7.addChild(valueUID2);
        
        contentContainer.addChild(subContainer7);
        addDivider(contentContainer,20);
        
        var subContainer8 = new FlexLayout();
        subContainer8.flexDirection = FlexLayout.FlexDirection.ROW;
        subContainer8.alignItems = FlexLayout.AlignItems.STRETCH;
        subContainer8.height = 60;
        
        var titleUID3 = new Label();
        titleUID3.font =  Font.create("Lato",16,Font.NORMAL);
        titleUID3.backgroundColor = Color.TRANSPARENT;
        titleUID3.textColor = Color.BLACK;
        titleUID3.textAlignment = TextAlignment.MIDLEFT;
        titleUID3.text = lang['pgSettings.uid'];
        titleUID3.margin = 10;
        titleUID3.flexGrow = 1;
        subContainer8.addChild(titleUID3);
        
        var valueUID3 = new Label();
        valueUID3.font =  Font.create("Lato",12,Font.NORMAL);
        valueUID3.backgroundColor = Color.TRANSPARENT;
        valueUID3.textColor = Color.create("#50000000");
        valueUID3.text = Hardware.UID
        valueUID3.margin = 15;
        subContainer8.addChild(valueUID3);
        
        contentContainer.addChild(subContainer8);
        addDivider(contentContainer,20)
        
        this.layout.addChild(container)
        
        function addDivider(container,rightLeftMargin)
        {
            var divider = new AbsoluteLayout();
            divider.height = 1;
            divider.backgroundColor = Color.create('#20000000');
            divider.alignSelf = FlexLayout.AlignSelf.STRETCH;
            divider.marginTop = 5;
            divider.marginBottom = 5;
            if(rightLeftMargin)
            {
                divider.marginRight = rightLeftMargin;
                divider.marginLeft = rightLeftMargin;
            }
            container.addChild(divider);
        }
        
    }});
    
});    

module.exports = pgSettings;
