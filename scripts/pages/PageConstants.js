const PageConstants = { }

PageConstants.PAGE_LOGIN = "login"; 
PageConstants.PAGE_PROFILE = "profile"; 
PageConstants.PAGE_MESSAGES = "messages"; 
PageConstants.PAGE_RESTAURANT = "resturant"; 
PageConstants.PAGE_RESTAURANT_DETAIL = "restaurant_detail"; 
PageConstants.PAGE_MAPVIEW = "mapView"; 
PageConstants.PAGE_SETTINGS = "settings"; 
PageConstants.PAGE_NOTIFICATIONS = "notifications"; 
PageConstants.PAGE_WEBVIEW = "webview"; 

module.exports = PageConstants;