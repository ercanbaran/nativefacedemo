/* 
		WARNING 
		Auto generated file. 
		Do not modify it's contents.
*/

var newPage1 = new SMF.UI.Page({
	name: "newPage1",
	showStatusBar: true,
	showNavigationBar: true,
	showActionBar: true
});



newPage1.onShow = function() {
    SMF.UI.statusBar.name = "statusBar";
    SMF.UI.statusBar.visible = true;
    if(Device.deviceOS === 'iOS'){
        SMF.UI.iOS.NavigationBar.name = "navigationBar";
        SMF.UI.iOS.NavigationBar.title = "newPage1";
        SMF.UI.iOS.NavigationBar.visible = true;
        page1.navigationItem.title =  "newPage1";
    }
    if(Device.deviceOS === 'Android'){
        newPage1.actionBar.name = "actionBar";
        newPage1.actionBar.title = "newPage1";
        newPage1.actionBar.visible = true;
    }
};

Object.assign(newPage1, {
    newPage1: newPage1
});

module && (module.exports = newPage1);