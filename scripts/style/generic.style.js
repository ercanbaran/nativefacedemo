const ImageFillType     = require("nf-core/ui/imagefilltype");
const Image             = require('nf-core/ui/image');
const Color             = require("nf-core/ui/color")

module.exports = {
    ".bold": {
        font: {
            bold: true
        },
    },
    ".Generic": {
        ".bg": {
            bottom: 0, 
            right: 0, 
            left: 0, 
            top:0,
            imageFillType : ImageFillType.STRETCH,
        },
        ".fillAbsoluteParent": {
            bottom: 0, 
            right: 0, 
            left: 0, 
            top:0,
        },
    }
}


/* 
Application.homeBackgroundImage
Application.positiveColor
Application.negativeColor
*/
