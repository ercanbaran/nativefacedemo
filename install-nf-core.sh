# Installing nf-core with js-base

checkExitCode() {
    # Reads last process' exit code and if it failed runs given parameters
    # prints error to output and exists process.
    if [ $? != 0 ]; then
        printf "ERROR\nCouldn't complete process, see logs below.\n"
        cat .stderr
        if [ "$@" != "" ]; then
            silent "$@"
        fi
        exit 1
    fi
}

silent() {
    "$@" >.stdout 2>.stderr
}

showUsage() {
    printf "Usage: ./install-nf-core.sh <alpha|beta|release>\n"
}

cleanJsBase() {
    if [ -e ./js-base ]; then
        rm -rf ./js-base
    fi
}

CHANNEL=$1
if [ "${1}" = "" ]; then 
    showUsage
    exit 1
fi

# Install nf-core

printf "Installing nf-core on ${CHANNEL}..."
## Backup npmrc
cp ~/.npmrc ~/.npmrc.bak

## Set nexus npm repository to registry
npm config set registry http://cd.smartface.io/nexus/content/repositories/smartfacenpm
npm config set _auth ZGVwbG95ZXI6M211bDR0MHIyMDE2
npm config set always-auth true

## Install sf-core
mkdir -p ./scripts/node_modules
silent pushd ./scripts/node_modules

if [ "${CHANNEL}" = "release" ]; then
    silent npm install nf-core
else
    silent npm install nf-core@${CHANNEL}
fi
checkExitCode

rm .stdout
rm .stderr

silent popd

printf "DONE\n"

## Recover npmrc
printf "Cleaning temporaries..."
cp ~/.npmrc.bak ~/.npmrc && rm ~/.npmrc.bak
rm .stdout
rm .stderr
printf "DONE\n"

# Install js-base

printf "Installing Js-Base..."

## Clone js-base repository
silent git clone https://github.com/smartface/js-base.git
checkExitCode cleanJsBase

## Install js-base
silent pushd js-base

silent npm install
checkExitCode cleanJsBase 

chmod +x install.sh
silent ./install.sh
checkExitCode cleanJsBase

silent popd

printf "DONE\n"

## Remove js-base folder
printf "Cleaning temporaries..."
silent cleanJsBase
printf "DONE\n"