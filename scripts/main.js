/* globals lang */
require('babel-polyfill/dist/polyfill.js');
require('js-extended-prototypes/extendPrototype.js');


const Pages = require('nf-core/ui/pages');
const FlexLayout        = require('nf-core/ui/flexlayout');
const Color             = require('nf-core/ui/color');
const ImageView         = require('nf-core/ui/imageview');
const Image             = require('nf-core/ui/image');
const Font              = require('nf-core/ui/font');
//const ImageFillType     = require("nf-core/ui/imagefilltype");
const Label             = require("nf-core/ui/label");
const TextAlignment     = require('nf-core/ui/textalignment');
const ListView          = require('nf-core/ui/listview');
const ListViewItem      = require("nf-core/ui/listviewitem");
const SliderDrawer      = require('nf-core/ui/sliderdrawer');
const PageConstants     = require('pages/PageConstants');
const System            = require("nf-core/device/system");
const Router            = require('nf-core/ui/router');


//Styler
const merge = require('deepmerge');
const styler = require("js-base/core/styler").styler;
const styleGeneric = require('./style/generic.style.js');
const stylePgLogin = require('./style/pgLogin.style.js');
const stylePgProfile = require('./style/pgProfile.style.js');
const styleOSSpecific = (Device.deviceOS === 'iOS') ? require('./style/ios.style.js') : require('./style/android.style.js');
var mergedStyle = merge.all([styleGeneric, stylePgLogin, stylePgProfile]);
styler(mergedStyle);

/**
 * Triggered when application is started.
 * @param {EventArguments} e Returns some attributes about the specified functions
 * @this Application
 */
  Router.add(PageConstants.PAGE_LOGIN, require('pages/pgLogin'));
    Router.add(PageConstants.PAGE_PROFILE, require('pages/pgProfile'));
    Router.add(PageConstants.PAGE_MESSAGES, require('pages/pgMessages'));
    Router.add(PageConstants.PAGE_RESTAURANT, require('pages/pgRestaurants'));
    Router.add(PageConstants.PAGE_RESTAURANT_DETAIL, require('pages/pgRestaurantDetail'));
    Router.add(PageConstants.PAGE_MAPVIEW, require('pages/pgMapView'));
    Router.add(PageConstants.PAGE_SETTINGS, require('pages/pgSettings'));
    Router.add(PageConstants.PAGE_NOTIFICATIONS, require('pages/pgNotifications'));
    Router.add(PageConstants.PAGE_WEBVIEW, require('pages/pgWebView'));
    
     Router.go(PageConstants.PAGE_LOGIN)

addSliderDrawer();

    //global.Pages.currentTag = PageConstants.PAGE_PROFILE;
    
        // Check for permissions & RAU updates
    // if (Device.deviceOS == "android") {
    // 	Permissions.checkPermission("WRITE_EXTERNAL_STORAGE", function(err) {
    // 		if (!err) checkforUpdate();
    // 	});
    // }
    // else {
    // 	setTimeout(function() {
    // 		checkforUpdate();
    // 	}, 3000);
    // }


function addSliderDrawer()
{
    var sliderDrawer = new SliderDrawer();
    sliderDrawer.width = 250;
    if(System.language.indexOf("ar") != -1)
    {
        sliderDrawer.drawerPosition = SliderDrawer.Position.RIGHT;
    }else
    {
        sliderDrawer.drawerPosition = SliderDrawer.Position.LEFT;

    }
    
    
    sliderDrawer.onLoad = function()
    {
            console.log("test2")

        sliderDrawer.layout.justifyContent = FlexLayout.JustifyContent.CENTER;
        sliderDrawer.layout.flexDirection =  FlexLayout.FlexDirection.COLUMN;
        sliderDrawer.layout.backgroundColor = Color.createGradient({startColor: Color.create("#9D1B55"), endColor:Color.create("#D9595B"), direction: Color.GradientDirection.DIAGONAL_LEFT});

        var image1 = Image.createFromFile("images://Avatar.png");
        var iconMessages    = Image.createFromFile("images://iconmessages.png");
        var iconProfile     = Image.createFromFile("images://iconprofile.png");
        var iconSettings    = Image.createFromFile("images://iconsettings.png");
        var iconRestaurants    = Image.createFromFile("images://food.png");
        var iconNotifications    = Image.createFromFile("images://iconmail.png");
        
        var myDataSet = [
                { 
                    title: lang["pgRestaurants.title"], 
                    icon: iconRestaurants,
                    tag : PageConstants.PAGE_RESTAURANT
                },
                { 
                    title: lang["pgMessages.title"], 
                    icon: iconMessages,
                    tag : PageConstants.PAGE_MESSAGES
                },
                { 
                    title: lang["pgProfile.title"], 
                    icon: iconProfile,
                    tag : PageConstants.PAGE_PROFILE
                },
                { 
                    title: lang["pgNotifications.title"], 
                    icon: iconNotifications,
                    tag : PageConstants.PAGE_NOTIFICATIONS
                },
                { 
                    title: lang["pgSettings.title"], 
                    icon: iconSettings,
                    tag : PageConstants.PAGE_SETTINGS
                },
                
            ]
       
       
        var topContainer = new FlexLayout();
        topContainer.flexGrow = 1.2;
        topContainer.flexBasis = 1;
        topContainer.justifyContent = FlexLayout.JustifyContent.CENTER;
        topContainer.alignItems     = FlexLayout.AlignItems.CENTER;
        topContainer.padding        = 10;
        topContainer.marginTop = 20;
        
        var profileImage = new ImageView();
        profileImage.image = Image.createFromFile("images://Avatar.png");
        profileImage.maxHeight = 100;
        profileImage.imageFillType =  ImageView.FillType.ASPECTFIT;
        //profileImage.alignSelf = FlexLayout.AlignSelf.STRETCH;
        
        topContainer.addChild(profileImage);
        
        var dividerTop = new FlexLayout();
        dividerTop.height = 1;
        dividerTop.backgroundColor = Color.create('#20FFFFFF');
        
       
        var listView = new ListView({
            itemCount: myDataSet.length,
            rowHeight: 60,
            flexGrow : 2,
            backgroundColor : Color.TRANSPARENT,
            onRowCreate: function(){
                var rowImage = new ImageView({
                    id: 3,
                    width : 50,
                    imageFillType: ImageView.FillType.ASPECTFIT,
                    margin : 10,
                })
                
                var rowTitle = new Label();
                rowTitle.flexGrow = 1;
                rowTitle.flexBasis = 1;
                rowTitle.id = 4;
                rowTitle.font =  Font.create("Lato",18,Font.NORMAL);
                rowTitle.backgroundColor = Color.TRANSPARENT;
                rowTitle.textColor = Color.WHITE;
                rowTitle.textAlignment = TextAlignment.MIDLEFT;
                rowTitle.alignSelf = FlexLayout.AlignSelf.CENTER;

                var rowTemplate = new ListViewItem({});
                rowTemplate.alignItems = FlexLayout.AlignItems.STRETCH;
                rowTemplate.flexDirection = FlexLayout.FlexDirection.ROW;
                rowTemplate.paddingLeft = 10;
                rowTemplate.paddingTop = 5;
                rowTemplate.paddingBottom = 5;
                
                rowTemplate.alpha = 0.5;
                rowTemplate.addChild(rowImage);
                rowTemplate.addChild(rowTitle);

                return rowTemplate;
            },
            onRowBind: function(listViewItem, index) {
                var rowTitle = listViewItem.findChildById(4);
                rowTitle.text = myDataSet[index].title;
                
                var rowImage = listViewItem.findChildById(3)
                rowImage.image = myDataSet[index].icon;
                
                if(Router.getCurrent() == myDataSet[index].tag)
                {
                    listViewItem.alpha = 1;
                }else
                {
                    listViewItem.alpha = 0.5;
                }

            },
            onRowSelected: function(listViewItem, index) {
                if(Router.getCurrent() != myDataSet[index].tag)
                {
                    
                    //global.Pages.currentTag = myDataSet[index].tag
                    //var nextpage = new (require(myDataSet[index].url));
                    //global.Pages.push(nextpage, false);
                    Router.go(myDataSet[index].tag,{},false);
                }
                listView.refreshData();
                
            },
            
            onPullRefresh: function(){
                //listView.itemCount = listView.itemCount + 10;
                //listView.refreshData();
                listView.stopRefresh();
            }
        });
        
        var dividerBottom = new FlexLayout();
        dividerBottom.height = 1;
        dividerBottom.backgroundColor = Color.create('#20FFFFFF');
        
        
        var btnSignOut = new Label();
        btnSignOut.height = 60;
        btnSignOut.font =  Font.create("Lato",16,Font.NORMAL);
        btnSignOut.backgroundColor = Color.TRANSPARENT;
        btnSignOut.textColor = Color.WHITE;
        btnSignOut.textAlignment = TextAlignment.MIDLEFT;
        btnSignOut.text = lang['pgSliderDrawer.signout'];
        btnSignOut.alpha = 0.5;
        btnSignOut.marginLeft = 30;
        btnSignOut.touchEnabled = true;
        
        btnSignOut.onTouch = function ()
        {
            //var nextpage = new(require('pages/pgLogin'));
            //global.Pages.sliderDrawer.hide();
            //global.Pages.push(nextpage,false)
            Router.go(PageConstants.PAGE_LOGIN);
        }
                    
        sliderDrawer.layout.addChild(topContainer);      
        sliderDrawer.layout.addChild(dividerTop)     
        sliderDrawer.layout.addChild(listView);
        sliderDrawer.layout.addChild(dividerBottom);   
        sliderDrawer.layout.addChild(btnSignOut);
    }
    
   Router.sliderDrawer = sliderDrawer;
   Router.sliderDrawer.enabled = false;

   //Pages.sliderDrawer = sliderDrawer;
   //sliderDrawer.show();
}


// Remote App Update update check function
function checkforUpdate() {
	//Checks if there is a valid update. If yes returns result object.     
	Application.checkUpdate(function(err, result) {
		if (err) {
			//Checking for update is failed
			console.log("rau error" + err)
		}
		else {
			//Update is successful. We can show the meta info to inform our app user.
			if (result.meta) {
				var isMandatory = (result.meta.isMandatory && result.meta.isMandatory === true) ? true : false;

				var updateTitle = (result.meta.title) ? result.meta.title : 'A new update is ready!';

				var updateMessage = "Version " + result.newVersion + " is ready to install.\n\n" +
					"What's new?:\n" + JSON.stringify(result.meta.update) +
					"\n\n"

				//adding mandatory status message.
				updateMessage += (isMandatory) ? "This update is mandatory!" : "Do you want to update?";

				//Function will run on users' approve
				function onFirstButtonPressed() {
					if (result.meta.redirectURL && result.meta.redirectURL.length != 0) {
						//RaU wants us to open a URL, most probably core/player updated and binary changed. 
						SMF.Net.browseOut(result.meta.redirectURL);
					}
					else {
						//Dialog.showWait();
						//There is an update waiting to be downloaded. Let's download it.
						result.download(function(err, result) {
							if (err) {
								//Download failed
								//Dialog.removeWait();
								alert("Autoupdate download failed: " + err);
							}
							else {
								//All files are received, we'll trigger an update.
								result.updateAll(function(err) {
									if (err) {
										//Updating the app with downloaded files failed
										//Dialog.removeWait();
										alert("Autoupdate update failed: " + err);
									}
									else {
										//After that the app will be restarted automatically to apply the new updates
										Application.restart();
									}
								});
							}
						});
					}
				}

				//We will do nothing on cancel for the timebeing.
				function onSecondButtonPressed() {
					//do nothing
				}

				//if Update is mandatory we will show only Update now button.
				if (isMandatory) {
					alert({
						title: updateTitle,
						message: updateMessage,
						firstButtonText: "Update now",
						onFirstButtonPressed: onFirstButtonPressed
					});
				}
				else {
					alert({
						title: updateTitle,
						message: updateMessage,
						firstButtonText: "Update now",
						secondButtonText: "Later",
						onFirstButtonPressed: onFirstButtonPressed,
						onSecondButtonPressed: onSecondButtonPressed

					});
				}
			}

		}
	});
}
