const ImageFillType     = require("nf-core/ui/imagefilltype");
const Color             = require("nf-core/ui/color");
const KeyboardType      = require('nf-core/ui/keyboardtype');

module.exports = {
    ".pgLogin": {
        ".iconSocial": {
            width: 50,
            height: 50,
            imageFillType: ImageFillType.ASPECTFIT
        },
        ".inputContainer": {
            height: 50,
            width: 350,
            margin: 10,
            borderWidth : 1,
            borderRadius : 10,
        },
        ".inputText": {
            flexGrow :1,
            keyboardType: KeyboardType.DEFAULT,
            margin : 5
        }
    }
};
