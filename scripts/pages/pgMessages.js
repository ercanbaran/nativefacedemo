/* globals */
const Page              = require("nf-core/ui/page");
const extend            = require("js-base/core/extend");
const componentStyler   = require("js-base/core/styler").componentStyler();
const FlexLayout        = require('nf-core/ui/flexlayout');
const AbsoluteLayout    = require('nf-core/ui/absolutelayout');
const Color             = require('nf-core/ui/color');
const ImageView         = require('nf-core/ui/imageview');
const Image             = require('nf-core/ui/image');
const Button            = require('nf-core/ui/button');
const Font              = require('nf-core/ui/font');
const Label             = require("nf-core/ui/label");
const ListView          = require('nf-core/ui/listview');
const ListViewItem      = require('nf-core/ui/listviewitem');
const TextAlignment     = require('nf-core/ui/textalignment');
const HeaderBarItem     = require('nf-core/ui/headerbaritem');
const AlertView         = require('nf-core/ui/alertview');
const StatusBarStyle    = require("nf-core/ui/statusbarstyle");
const Direction         = require("nf-core/ui/listview/direction");
const Contacts          = require("nf-core/device/contacts");
const SearchView        = require('nf-core/ui/searchview');
const ActivityIndicator = require('nf-core/ui/activityindicator');
const Timer             = require("nf-core/global/timer");
const Screen            = require("nf-core/device/screen");
const Router            = require('nf-core/ui/router');
const PageConstants     = require('pages/PageConstants');

var pgMessages = new extend(Page)(
    function(_super,params)
    {
        var self = this;
        _super(this,{
    onShow : function()
        {
            this.headerBar.title = lang['pgMessages.title'];
            
            //pgMessages.statusBar.ios.style = StatusBarStyle.LIGHTCONTENT;
            this.headerBar.displayShowHomeEnabled = true;
            this.headerBar.android.displayShowHomeEnabled = true;
            this.statusBar.ios.style = StatusBarStyle.LIGHTCONTENT;

        },
    onLoad : function(){
       
        this.headerBar.visible = true;
        this.headerBar.backgroundColor = Color.create("#9D1B55");
        this.headerBar.titleColor = Color.WHITE;
        this.headerBar.itemColor = Color.WHITE;
        this.layout.backgroundColor = Color.createGradient({startColor: Color.create("#9D1B55"), endColor:Color.create("#D9595B"), direction: Color.GradientDirection.DIAGONAL_LEFT});

        
        var addItem = new HeaderBarItem({
            //title: "Smartface",
            image: Image.createFromFile("images://Pluss.png"),
            color : Color.WHITE,
            onPress: function() {
                showAlertDialog()
                
            }
        });
        this.headerBar.setItems([addItem]);
        
        var menuIcon = new HeaderBarItem({
            color : Color.WHITE,
            image: Image.createFromFile("images://Menuiconn.png"),
            onPress: function() {
                Router.sliderDrawer.show();
            }
        });
        this.headerBar.setLeftItem(menuIcon);
        
        
        var image1 = Image.createFromFile("images://Avatar2.png");
        var image2 = Image.createFromFile("images://Avatar3.png");
        var image3 = Image.createFromFile("images://Avatar4.png");
        var image4 = Image.createFromFile("images://Avatar5.png");
        var image5 = Image.createFromFile("images://Avatar6.png");
        var image6 = Image.createFromFile("images://Avatar7.png");


        
        var myDataSet = [
                    { 
                        message: 'What are you talking about? ', 
                        from: 'Dennis Howard',
                        date : '5 MIN AGO',
                        image : image1,
                    },
                    { 
                        message: 'I do not wanna see you again.', 
                        from: 'Samuel Shaw',
                        date : 'YESTERDAY',
                        image : image2,
                    },
                    { 
                        message: 'What happens to US in the future? ', 
                        from: 'Tyler Perry',
                        date : 'JULY 3',
                        image : image3,
                    },
                    { 
                        message: 'I\'m gonna read your thoughts. ', 
                        from: 'Patrick Owens',
                        date : 'APRIL 7',
                        image : image4,
                    },
                    { 
                        message: 'Don\'t worry. ', 
                        from: 'Linda Wade',
                        date : 'MARCH 14',
                        image : image5,
                    },
                    { 
                        message: 'I just got here, okay ', 
                        from: 'Adam Carpenter',
                        date : 'JANUARY 18',
                        image : image6,
                    }
            ]
       
        var listView = new ListView({
            itemCount: 0,
            rowHeight: 80,
            flexGrow:1,
            backgroundColor : Color.TRANSPARENT,
            
            onRowCreate: function(){
                
                var thumb = new ImageView();
                thumb.id = 2;
                thumb.imageFillType = ImageView.FillType.ASPECTFIT;
                thumb.maxHeight = 70;
                thumb.marginTop = 5;
                thumb.marginBottom = 5;
                
                var textContainer = new FlexLayout();
                textContainer.id = 3;
                textContainer.flexGrow = 1;
                textContainer.flexBasis = 1;
                //textContainer.backgroundColor = Color.BLUE
                textContainer.alignItems = FlexLayout.AlignItems.STRETCH;
                
                var nameContainer = new FlexLayout();
                nameContainer.id = 31;
                nameContainer.flexGrow = 1;
                nameContainer.flexBasis = 1;
                nameContainer.flexDirection = FlexLayout.FlexDirection.ROW;
                nameContainer.alignItems = FlexLayout.AlignItems.STRETCH;

                
                var nameLabel = new Label();
                nameLabel.flexGrow = 1;
                nameLabel.flexBasis = 1;
                nameLabel.id = 311;
                nameLabel.font =  Font.create("Lato",16,Font.NORMAL);
                nameLabel.backgroundColor = Color.TRANSPARENT;
                nameLabel.textColor = Color.WHITE;
                nameLabel.textAlignment = TextAlignment.MIDLEFT;
                nameLabel.alignSelf = FlexLayout.AlignSelf.CENTER;
                nameContainer.addChild(nameLabel);
                
                var dateLabel = new Label();
                dateLabel.id = 312;
                dateLabel.font =  Font.create("Lato",10,Font.NORMAL);
                dateLabel.backgroundColor = Color.TRANSPARENT;
                dateLabel.textColor = Color.WHITE;
                dateLabel.textAlignment = TextAlignment.MIDRIGHT;
                dateLabel.alpha = 0.5
                dateLabel.marginRight = 5;
                dateLabel.minWidth = 75;
                nameContainer.addChild(dateLabel);
                
                
                textContainer.addChild(nameContainer);
                
                var message = new Label();
                message.id = 32;
                message.flexGrow = 1;
                message.flexBasis = 1;
                message.font =  Font.create("Lato",12,Font.NORMAL);
                message.backgroundColor = Color.TRANSPARENT;
                message.textColor = Color.WHITE;
                message.textAlignment = TextAlignment.TOPLEFT;
                message.alpha = 0.5
                textContainer.addChild(message);

                
                var rowTemplate = new ListViewItem({});
                rowTemplate.alignItems = FlexLayout.AlignItems.STRETCH;
                rowTemplate.flexDirection = FlexLayout.FlexDirection.ROW;
                rowTemplate.addChild(thumb);
                rowTemplate.addChild(textContainer);
                
                return rowTemplate;
            },
            onRowBind: function(listViewItem, index) {
                var thumb = listViewItem.findChildById(2)
                thumb.image = myDataSet[index%6].image;
                
                
                var nameLabel = listViewItem.findChildById(3).findChildById(31).findChildById(311)
                nameLabel.text = myDataSet[index%6].from;
                
                var dateLabel = listViewItem.findChildById(3).findChildById(31).findChildById(312)
                dateLabel.text = myDataSet[index%6].date;
                
                
                var messageLabel = listViewItem.findChildById(3).findChildById(32)
                messageLabel.text = myDataSet[index%6].message;
                
               

            },
            onPullRefresh: function(){
                //listView.itemCount = listView.itemCount + 10;
                //listView.refreshData();
                listView.stopRefresh();
            }
        });
        
        listView.ios.onRowSwiped = function(direction,expansionSettings){
             if (direction == Direction.RIGHTTOLEFT){// left
             
                expansionSettings.fillOnTrigger = true;
                expansionSettings.threshold = 1.5;
                var deleteAction = new listView.ios.swipeItem(lang['pgMessages.delete'],Color.RED,15,function(e) { 
                    myDataSet.splice(e.index,1);
                    listView.itemCount = myDataSet.length;
                    listView.deleteRow(e.index);
                 });
                 
                 var replyAction = new listView.ios.swipeItem(lang['pgMessages.reply'],Color.GRAY,15,function(e){
                 });
                 
                 return [deleteAction,replyAction];
                 
             }else if (direction == Direction.LEFTTORIGHT){
                 var replyAction = new listView.ios.swipeItem(lang['pgMessages.more'],Color.GRAY,15,function(e){
                 });
                 return [replyAction];
             }
        };
        
        var myActivityIndicator = new ActivityIndicator({
          positionType : FlexLayout.PositionType.ABSOLUTE,
          color : Color.WHITE,
          top: Screen.height/2 - 20,
          left: Screen.width/2 - 20
        });
        
        this.layout.addChild(myActivityIndicator);
        
        var myTimer = Timer.setTimeout({
            task: addListView,
            delay: 2000 
            });
            
        function addListView()
        {
             self.layout.removeChild(myActivityIndicator);
              listView.itemCount = myDataSet.length;
              listView.refreshData();
        }        
        
                    
        this.layout.addChild(listView);
        
        var searchBar = new SearchView();
        searchBar.onSearchButtonClicked = function(searchText){
            
            searchBar.hideKeyboard();
            // myDataSet = search(searchBar.text,myDataSet);
            // listView.itemCount = myDataSet.length;
            // listView.refreshData();
        };
        searchBar.addToHeaderBar(this);
        
        function search(text, data) {
            return data.reduce(function(acc, value) {
                if (value.from.toLowerCase().includes(text.toLowerCase())) {
                    acc.push(value);
                }
                return acc;
            }, []);
        }
            

        function showAlertDialog()
        {
            var myAlertView = new AlertView({
                title: "NativeFace",
                message: "Do you want to add NF-Core Team to your contacts?"
            });
            myAlertView.addButton({
                index: AlertView.ButtonType.NEGATIVE, 
                text: lang.cancel
            });
            myAlertView.addButton({
                index: AlertView.ButtonType.POSITIVE, 
                text: lang.ok,
                onClick: function() {
                    Contacts.add({
        	    	displayName : "NF-Core Team",
        	    	phoneNumber : "0532 725 16 40",
        	    	email		: "test@smartface.io",
        	    	address		: "347 N Canon Dr Beverly Hills, CA 90210",
        	    	page       : self,
        	    	onSuccess : function(){
        	    		console.log("Success");
        	    	},
        	    	onFailure : function(){
        	    		console.log("Failure");
        	    	}
        	    });
                }
            });
            
            myAlertView.show();
        }
    }});
    
});    

module.exports = pgMessages;
