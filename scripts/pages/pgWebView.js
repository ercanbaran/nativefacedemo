/* globals */
const Page       = require("nf-core/ui/page");
const extend     = require("js-base/core/extend");
const FlexLayout = require('nf-core/ui/flexlayout');
const Color      = require('nf-core/ui/color');
const Button     = require('nf-core/ui/button');
const Animator   = require('nf-core/ui/animator');
const WebView    = require('nf-core/ui/webview');
const Router            = require('nf-core/ui/router');
const PageConstants     = require('pages/PageConstants');

var pgWebView = extend(Page)(
    function(_super,params)
    {
    	var self = this;
        _super(this, 
        {
		    onShow : function(){
		    	self.headerBar.visible = true;
                self.headerBar.backgroundColor = Color.create("#9D1B55");
                self.headerBar.titleColor = Color.WHITE;
                self.headerBar.itemColor = Color.WHITE;
                self.headerBar.title = lang['pgRestaurantDetail.title'];
                self.headerBar.displayShowHomeEnabled = true;
		    },
		    onLoad : function(){
		        var myWebView = new WebView({
                    flexGrow:1
                });
                myWebView.loadURL('http://www.zomato.com');

		        self.layout.addChild(myWebView);
		    }
        });
	}
);  
module.exports = pgWebView;
