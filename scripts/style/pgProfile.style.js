const ImageFillType     = require("nf-core/ui/imagefilltype");
const Color             = require("nf-core/ui/color");
const Font              = require("nf-core/ui/font");
const TextAlignment     = require('nf-core/ui/textalignment');


module.exports = {
    ".pgProfile": {
        ".iconSocial": {
            width: 50,
            height: 50,
            imageFillType: ImageFillType.ASPECTFIT
        },
        ".boxTextLarge": {
            top: 0,
            left:0,
            right:0,
            textAlignment : TextAlignment.TOPCENTER
        },
        ".boxTextSmall": {
            bottom: 0,
            left:0,
            right:0,
            touchEnabled : false,
            textAlignment : TextAlignment.TOPCENTER
        }
    }
};
