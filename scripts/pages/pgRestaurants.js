/* globals */
const Page              = require("nf-core/ui/page");
const extend            = require("js-base/core/extend");
const componentStyler   = require("js-base/core/styler").componentStyler();
const FlexLayout        = require('nf-core/ui/flexlayout');
const AbsoluteLayout    = require('nf-core/ui/absolutelayout');
const Color             = require('nf-core/ui/color');
const ImageView         = require('nf-core/ui/imageview');
const Image             = require('nf-core/ui/image');
const Button            = require('nf-core/ui/button');
const Font              = require('nf-core/ui/font');
const Label             = require("nf-core/ui/label");
const ListView          = require('nf-core/ui/listview');
const ListViewItem      = require('nf-core/ui/listviewitem');
const TextAlignment     = require('nf-core/ui/textalignment');
const HeaderBarItem     = require('nf-core/ui/headerbaritem');
const AlertView         = require('nf-core/ui/alertview');
const StatusBarStyle    = require("nf-core/ui/statusbarstyle");
const Direction         = require("nf-core/ui/listview/direction");
const Http              = require("nf-core/net/http")
const ActivityIndicator = require('nf-core/ui/activityindicator');
const Timer             = require("nf-core/global/timer");
const Screen            = require("nf-core/device/screen");
const Router            = require('nf-core/ui/router');
const PageConstants     = require('pages/PageConstants');

var pgRestaurants = new extend(Page)(
    function(_super,params)
    {
        var self = this;
        _super(this,{
            
        
    onShow : function()
        {
            this.headerBar.title = lang['pgRestaurants.title'];
            this.headerBar.displayShowHomeEnabled = true;
            this.headerBar.android.displayShowHomeEnabled = true;
            this.statusBar.ios.style = StatusBarStyle.LIGHTCONTENT;

        },
    onLoad : function(){
       
        this.headerBar.visible = true;
        this.headerBar.backgroundColor = Color.create("#9D1B55");
        this.headerBar.titleColor = Color.WHITE;
        this.headerBar.itemColor = Color.WHITE;
        
        this.layout.backgroundColor = Color.createGradient({startColor: Color.create("#9D1B55"), endColor:Color.create("#D9595B"), direction: Color.GradientDirection.DIAGONAL_LEFT});
        
        var menuIcon = new HeaderBarItem({
            color : Color.WHITE,
            image: Image.createFromFile("images://Menuiconn.png"),
            onPress: function() {
                Router.sliderDrawer.show();
            }
        });
        this.headerBar.setLeftItem(menuIcon);
        
        
        var image1 = "http://ref.smartface.io/testimages/restaurant1.png"
        var image2 = "http://ref.smartface.io/testimages/restaurant2.png"
        var image3 = "http://ref.smartface.io/testimages/restaurant3.png"
        var image4 = "http://ref.smartface.io/testimages/restaurant4.png"
        var image5 = "http://ref.smartface.io/testimages/restaurant5.png"
        // var image1 = Image.createFromFile("images://restaurant1.png");
        // var image2 = Image.createFromFile("images://restaurant2.png");
        // var image3 = Image.createFromFile("images://restaurant3.png");
        // var image4 = Image.createFromFile("images://restaurant4.png");
        // var image5 = Image.createFromFile("images://restaurant5.png");
        var imageRating = Image.createFromFile("images://star.png");

        
        var myDataSet = [
                    { 
                        name: 'Love Story Cafe', 
                        rating: '4.9',
                        distance : '0.1 km away',
                        image : image1,
                    },
                    { 
                        name: 'Coffee Shop Unlimited', 
                        rating: '4.7',
                        distance : '0.3 km away',
                        image : image2,
                    },
                    { 
                        name: 'Things Cafe', 
                        rating: '4.2',
                        distance : '0.5 km away',
                        image : image3,
                    },
                    { 
                        name: 'John & Jack’s Pub', 
                        rating: '3.8',
                        distance : '1.2 km away',
                        image : image4,
                    },
                    { 
                        name: 'BBQ & Steak House', 
                        rating: '4.5',
                        distance : '3.1 km away',
                        image : image5,
                    }
            ]
       
        var listView = new ListView({
            flexGrow : 1,
            itemCount: 0,
            rowHeight: 140,
            backgroundColor:Color.TRANSPARENT,
            

            
            onRowCreate: function(){
                
                var rowFlex = new FlexLayout();
                rowFlex.id = 2
                rowFlex.flexDirection =  FlexLayout.FlexDirection.ROW;
                rowFlex.alignItems = FlexLayout.AlignItems.STRETCH;
                rowFlex.flexGrow = 1;
                rowFlex.backgroundColor = Color.WHITE;
                rowFlex.borderWidth =1;
                rowFlex.borderColor = Color.TRANSPARENT;
                rowFlex.borderRadius = 10;
                rowFlex.marginTop = 10;
                rowFlex.marginLeft = 10;
                rowFlex.marginRight = 10;

                
                var rowImage = new ImageView({
                    id: 3,
                    width :130,
                    height : 130,
                    imageFillType: ImageView.FillType.ASPECTFIT,
                })
                

                
                var textContainer1 = new FlexLayout();
                textContainer1.id = 4;
                textContainer1.flexGrow = 3;
                textContainer1.justifyContent = FlexLayout.JustifyContent.CENTER;
                textContainer1.paddingLeft = 10;

                var nameText = new Label();
                nameText.id = 44;
                nameText.font =  Font.create("Lato",20,Font.NORMAL);
                nameText.backgroundColor = Color.TRANSPARENT;
                nameText.textColor = Color.BLACK;
                nameText.marginBottom = 10;
                
                var distanceText = new Label();
                distanceText.id = 444;
                distanceText.font =  Font.create("Lato",14,Font.NORMAL);
                distanceText.backgroundColor = Color.TRANSPARENT;
                distanceText.textColor = Color.BLACK;
                distanceText.touchEnabled = false;
                distanceText.alpha = 0.5
                
                textContainer1.addChild(nameText);
                textContainer1.addChild(distanceText);
                
                var ratingContainer = new FlexLayout();
                ratingContainer.id = 5
                ratingContainer.flexDirection =  FlexLayout.FlexDirection.ROW;
                ratingContainer.alignItems = FlexLayout.AlignItems.STRETCH;
                ratingContainer.width = 50;
                ratingContainer.height = 26;
                ratingContainer.backgroundColor = Color.create("#FFA200");
                ratingContainer.positionType = FlexLayout.PositionType.ABSOLUTE;
                ratingContainer.right = 10;
                ratingContainer.top = 10;
                ratingContainer.borderWidth =1;
                ratingContainer.borderColor = Color.TRANSPARENT;
                ratingContainer.borderRadius = 13;
                ratingContainer.paddingRight = 5;
                ratingContainer.paddingLeft = 5;
                
                var ratingText = new Label();
                ratingText.id = 51;
                ratingText.font =  Font.create("Lato",12,Font.NORMAL);
                ratingText.backgroundColor = Color.TRANSPARENT;
                ratingText.textColor = Color.WHITE;
                ratingText.touchEnabled = false;
                ratingText.flexGrow =1;
                ratingText.textAlignment = TextAlignment.TOPRIGHT;
                ratingText.marginRight = 3;
                ratingText.alignSelf = FlexLayout.AlignSelf.CENTER;

                var ratingImage = new ImageView;
                ratingImage.id = 52;
                ratingImage.width = 15;
                ratingImage.imageFillType = ImageView.FillType.ASPECTFIT
                ratingImage.image = imageRating;
                
                
                ratingContainer.addChild(ratingText);
                ratingContainer.addChild(ratingImage);
                
                
                rowFlex.addChild(rowImage);
                rowFlex.addChild(textContainer1);
                rowFlex.addChild(ratingContainer);


                var rowTemplate = new ListViewItem({});
                rowTemplate.addChild(rowFlex);

                return rowTemplate;
            },
            onRowBind: function(listViewItem, index) {

                var rowImage = listViewItem.findChildById(2).findChildById(3);
                rowImage.image = undefined;
                rowImage.loadFromUrl("https://unsplash.it/300/300/?random=" + index)
                // Http.requestImage("https://unsplash.it/300/300/?random"/*myDataSet[index%5].image*/,onLoad,onError);
                // function onLoad(response) {
     
                //         rowImage.image = response;

                // }
                // function onError(error) {
                //     console.log("Error " + error);
                // }
                
                // var image = Image.createFromFile("images://Avatar6.png");
            
                // rowImage.loadFromUrl(imageArray[index%8],image);
                
                var nameText = listViewItem.findChildById(2).findChildById(4).findChildById(44);
                nameText.text = myDataSet[index%5].name;
                
                var distanceText = listViewItem.findChildById(2).findChildById(4).findChildById(444);
                distanceText.text = myDataSet[index%5].distance;
                
                var ratingText = listViewItem.findChildById(2).findChildById(5).findChildById(51);
                ratingText.text = myDataSet[index%5].rating;

            },
             onRowSelected: function(listViewItem, index) {
                Router.go(PageConstants.PAGE_RESTAURANT_DETAIL)

            },
            onPullRefresh: function(){
                //listView.itemCount = listView.itemCount + 10;
                //listView.refreshData();
                listView.stopRefresh();
            }
        });
        
        var myActivityIndicator = new ActivityIndicator({
          positionType : FlexLayout.PositionType.ABSOLUTE,
          color : Color.WHITE,
          top: Screen.height/2 - 20,
          left: Screen.width/2 - 20
        });
        
        this.layout.addChild(myActivityIndicator);
        
        var myTimer = Timer.setTimeout({
            task: addListView,
            delay: 2000 
            });
            
        function addListView()
        {
             self.layout.removeChild(myActivityIndicator);
             listView.itemCount = 200;
             listView.refreshData();
        }     
        
        
        this.layout.addChild(listView);
        
            

        
    }});
    
});    

var imageArray = ["http://cdn.skim.gs/images/rjb53vfydwrle5fkhr8d/food-porn-burgers-burgers-to-drool-over","https://s-media-cache-ak0.pinimg.com/originals/8a/e1/a8/8ae1a8abe2676f008e310100df0569e1.jpg","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRJTkGxzhDu3iCYyt2Qg8Yi8LKy_vBK-uQwac2SRjwxCgA30Rujsw","http://imgs.xkcd.com/comics/secretary_part_2.png","http://imgs.xkcd.com/comics/secretary_part_1.png","http://imgs.xkcd.com/comics/actuarial.png","http://imgs.xkcd.com/comics/election.png","http://imgs.xkcd.com/comics/scantron.png"];
module.exports = pgRestaurants;
