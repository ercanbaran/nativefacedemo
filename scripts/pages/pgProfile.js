/* globals */
const Page              = require("nf-core/ui/page");
const extend            = require("js-base/core/extend");
const componentStyler   = require("js-base/core/styler").componentStyler();
const FlexLayout        = require('nf-core/ui/flexlayout');
const AbsoluteLayout    = require('nf-core/ui/absolutelayout');
const Color             = require('nf-core/ui/color');
const ImageView         = require('nf-core/ui/imageview');
const Image             = require('nf-core/ui/image');
const Font              = require('nf-core/ui/font');
const Label             = require("nf-core/ui/label");
const TextBox           = require('nf-core/ui/textbox');
const TextAlignment     = require('nf-core/ui/textalignment');
const ListView          = require('nf-core/ui/listview');
const ListViewItem      = require("nf-core/ui/listviewitem");
const HeaderBarItem     = require("nf-core/ui/headerbaritem");
const AlertView         = require("nf-core/ui/alertview");
const StatusBarStyle    = require("nf-core/ui/statusbarstyle");
const SliderDrawer      = require('nf-core/ui/sliderdrawer');
const Share             = require('nf-core/share');
const Screen            = require("nf-core/device/screen");
const Multimedia        = require("nf-core/device/multimedia");
const ActivityIndicator = require('nf-core/ui/activityindicator');
const Timer             = require("nf-core/global/timer");
const Router            = require('nf-core/ui/router');
const PageConstants     = require('pages/PageConstants');


var pgProfile = new extend(Page)(
    function(_super,params)
    {
        var self = this;
        _super(this,{
    onShow : function()
        {
            this.headerBar.visible = true;
            this.headerBar.title = lang['pgProfile.title'];
            
            this.headerBar.displayShowHomeEnabled = true;
            this.statusBar.ios.style = StatusBarStyle.LIGHTCONTENT;
            Router.sliderDrawer.enabled = true

        },
    onLoad : function(){
        try{
        this.headerBar.visible = true;
        this.headerBar.title = lang['pgProfile.title'];
            
        this.headerBar.displayShowHomeEnabled = true;
        this.statusBar.android.color = Color.create("#9D1B55");
        this.statusBar.ios.style = StatusBarStyle.LIGHTCONTENT;
        this.headerBar.backgroundColor = Color.create("#9D1B55");
        this.headerBar.titleColor = Color.WHITE;
        this.headerBar.itemColor = Color.WHITE;
        var menuIcon = new HeaderBarItem({
            //title: "Menu",
            color : Color.WHITE,
            image: Image.createFromFile("images://Menuiconn.png"),
            onPress: function() {
                //showAlertDialog("Please be patient and wait until the end of Sprint 20 :)");
                Router.sliderDrawer.show();
            }
        });
        this.headerBar.setLeftItem(menuIcon);
        
        /*var settingMenuItem = new HeaderBarItem({
            color : Color.WHITE,
            image: Image.createFromFile("images://Cog.png"),
            onPress: function() {
                var nextpage = require('pages/pgSettings');
                global.Pages.push(nextpage, false);
            }
        });
        this.headerBar.setItems([settingMenuItem]);*/
        
        this.layout.backgroundColor = Color.createGradient({startColor: Color.create("#9D1B55"), endColor:Color.create("#D9595B"), direction: Color.GradientDirection.DIAGONAL_LEFT});

        

        var mainLayout = new FlexLayout({
            flexGrow : 1,
            flexDirection: FlexLayout.FlexDirection.COLUMN,
            //justifyContent : FlexLayout.JustifyContent.CENTER,
        });
        
        
        var topContainer = new FlexLayout({
            flexDirection: FlexLayout.FlexDirection.ROW,
            justifyContent : FlexLayout.JustifyContent.CENTER,
            alignItems : FlexLayout.AlignItems.CENTER,
            alignSelf : FlexLayout.AlignSelf.STRETCH,
            marginTop : 50,
            height : 100
            
        });
        
        var btnMessage = new ImageView();
        componentStyler(".pgProfile.iconSocial")(btnMessage);
        btnMessage.touchEnabled = true;
        btnMessage.image = Image.createFromFile("images://Message.png");
        btnMessage.onTouch = function(){
            Router.go(PageConstants.PAGE_MESSAGES)
        };
        
        var profileImage = new ImageView();
        profileImage.image = Image.createFromFile("images://Avatar.png");
        profileImage.imageFillType =  ImageView.FillType.ASPECTFIT;
        profileImage.alignSelf = FlexLayout.AlignSelf.STRETCH;//Android addition
        profileImage.touchEnabled = true;
        
        profileImage.onTouch = function()
        {

            //if(readExternalStoragePermission())  {
                Multimedia.pickFromGallery({
                    type: Multimedia.Type.IMAGE,
                    page: self,
                    onSuccess: onSuccess
                });
            //}
            
            function onSuccess(e) {
                console.log("res")
                var image = e.image;
                profileImage.image = image;
                profileImage.imageFillType = ImageView.FillType.ASPECTFIT
                //profileImage.applyLayout()
            }
            
            function readExternalStoragePermission() {
                var result = Application.checkPermission("READ_EXTERNAL_STORAGE");
                if(!result) {
                    var permissionCode = 1003;
                    Application.requestPermissions(permissionCode, "READ_EXTERNAL_STORAGE");
                }
                return Application.checkPermission("READ_EXTERNAL_STORAGE");
            }
        }


        var btnFollow = new ImageView();
        componentStyler(".pgProfile.iconSocial")(btnFollow);
        btnFollow.image = Image.createFromFile("images://Follow.png");
        btnFollow.touchEnabled = true;
        
        btnFollow.onTouch = function()
        {
            //var image = Screen.capture();
            //Share.shareImage(image, self, []);
            Share.shareText("Hello", self, []);
        }
        
        topContainer.addChild(btnMessage);
        topContainer.addChild(profileImage);
        topContainer.addChild(btnFollow);

        
        var followContainer = new FlexLayout();
        followContainer.flexGrow = 1.5;
        followContainer.flexBasis = 1;
        followContainer.flexDirection =  FlexLayout.FlexDirection.ROW;
        followContainer.justifyContent = FlexLayout.JustifyContent.CENTER;
        followContainer.alignItems = FlexLayout.AlignItems.CENTER;
        followContainer.alignSelf = FlexLayout.AlignSelf.STRETCH;//Android addition
        addBoxText('68',lang['pgProfile.following'],followContainer);
        addBoxDivider(followContainer);
        addBoxText('24',lang['pgProfile.albums'],followContainer);
        addBoxDivider(followContainer);
        addBoxText('715',lang['pgProfile.followers'],followContainer);
        
        
        
        var bottomContainer = new FlexLayout();
        bottomContainer.backgroundColor = Color.WHITE;
        bottomContainer.flexGrow = 5;
        bottomContainer.flexBasis = 1;
        bottomContainer.flexDirection =  FlexLayout.FlexDirection.COLUMN;
        
        var trackContainer = new FlexLayout();
        trackContainer.height = 50;
        trackContainer.flexDirection =  FlexLayout.FlexDirection.ROW;
        trackContainer.justifyContent = FlexLayout.JustifyContent.CENTER;
        trackContainer.alignItems = FlexLayout.AlignItems.CENTER;
        
        addTrackText('847 ' + lang['pgProfile.tracks'],trackContainer);
        addSmallDivider(trackContainer)
        addTrackText('26 ' + lang['pgProfile.playlists'],trackContainer);
        addSmallDivider(trackContainer)
        addTrackText('5472 ' + lang['pgProfile.likes'],trackContainer);
        bottomContainer.addChild(trackContainer);
        
        var dvd = new AbsoluteLayout();
        dvd.height = 1;
        dvd.alignSelf = FlexLayout.AlignSelf.STRETCH;
        dvd.backgroundColor = Color.create('#20000000');
        bottomContainer.addChild(dvd);
        var thumb1 = Image.createFromFile("images://Cover1.png");
        var thumb2 = Image.createFromFile("images://Cover2.png");
        var thumb3 = Image.createFromFile("images://Cover3.png");
        var thumb4 = Image.createFromFile("images://Cover4.png");
        
        
        var myDataSet = [
                    { 
                        title: 'Summer ', 
                        like: '549 Likes',
                        track: '25 Tracks >',
                        image : thumb1
                    },
                    { 
                        title: 'Road Trip', 
                        like: '482 Likes',
                        track: '54 Tracks >',
                        image : thumb2
                    },
                    { 
                        title: 'Night City', 
                        like: '143 Likes',
                        track: '44 Tracks >',
                        image : thumb3
                    },
                    { 
                        title: 'Winter Mix', 
                        like: '536 Likes',
                        track: '22 Tracks >',
                        image : thumb4
                    }
            ]
        
       
        

        var listView = new ListView({
            flexGrow : 1,
            itemCount: 0,
            rowHeight: 90,

            onRowCreate: function(){
                
                var rowFlex = new FlexLayout();
                rowFlex.id = 2
                rowFlex.flexDirection =  FlexLayout.FlexDirection.ROW;
                //rowFlex.alignSelf = FlexLayout.AlignSelf.STRETCH;
                rowFlex.alignItems = FlexLayout.AlignItems.STRETCH;
                rowFlex.flexGrow = 1;
                
                
                var rowImage = new ImageView({
                    id: 3,
                    width : 60,
                    height :60,
                    imageFillType: ImageView.FillType.ASPECTFIT,
                    margin : 10
                    //alignSelf : FlexLayout.AlignSelf.STRETCH
                })
                
                var textContainer1 = new FlexLayout();
                textContainer1.id = 4;
                textContainer1.flexGrow = 3;
                textContainer1.justifyContent = FlexLayout.JustifyContent.CENTER;
                //textContainer1.alignSelf = FlexLayout.AlignSelf.STRETCH

                var followTextLarge = new Label();
                followTextLarge.id = 44;
                followTextLarge.font =  Font.create("Lato",20,Font.NORMAL);
                followTextLarge.backgroundColor = Color.TRANSPARENT;
                followTextLarge.textColor = Color.BLACK;
                followTextLarge.marginBottom = 5;
                
                var followTextSmall = new Label();
                followTextSmall.id = 444;
                followTextSmall.font =  Font.create("Lato",12,Font.NORMAL);
                followTextSmall.backgroundColor = Color.TRANSPARENT;
                followTextSmall.textColor = Color.BLACK;
                followTextSmall.touchEnabled = false;
                followTextSmall.alpha = 0.5
                
                textContainer1.addChild(followTextLarge);
                textContainer1.addChild(followTextSmall);
                
                var trackText = new Label();
                trackText.id = 5;
                trackText.flexGrow = 1;
                trackText.font =  Font.create("Lato",16,Font.NORMAL);
                trackText.backgroundColor = Color.TRANSPARENT;
                trackText.textColor = Color.BLACK;
                trackText.alpha = 0.5
                //trackText.alignSelf = FlexLayout.AlignSelf.STRETCH

                
                rowFlex.addChild(rowImage);
                rowFlex.addChild(textContainer1);
                rowFlex.addChild(trackText);
                
                var divider = new AbsoluteLayout();
                divider.height = 1;
                divider.id = 6;
                divider.bottom = 0;
                divider.left = 0;
                divider.right = 0;
                divider.backgroundColor = Color.create('#20000000');

                var rowTemplate = new ListViewItem({});
                rowTemplate.addChild(rowFlex);
                rowTemplate.addChild(divider);

                return rowTemplate;
            },
            onRowBind: function(listViewItem, index) {
                var rowImage = listViewItem.findChildById(2).findChildById(3);
                rowImage.image = myDataSet[index%4].image;

                var titleText = listViewItem.findChildById(2).findChildById(4).findChildById(44);
                titleText.text = myDataSet[index%4].title;
                
                var likeText = listViewItem.findChildById(2).findChildById(4).findChildById(444);
                likeText.text = myDataSet[index%4].like;
                
                var trackText = listViewItem.findChildById(2).findChildById(5);
                trackText.text = myDataSet[index%4].track;

            },
            onPullRefresh: function(){
                //listView.itemCount = listView.itemCount + 10;
                //listView.refreshData();
                listView.stopRefresh();
            }
        });
        
        var myActivityIndicator = new ActivityIndicator({
          positionType : FlexLayout.PositionType.ABSOLUTE,
          color : Color.create("#9D1B55"),
          top: 120,
          left: Screen.width/2-10
        });
        
        
        
        var myTimer = Timer.setTimeout({
            task: addListView,
            delay: 2000 
            });
            
        function addListView()
        {
             bottomContainer.removeChild(myActivityIndicator);
             listView.itemCount = 200;
             listView.refreshData();
        }
        
        bottomContainer.addChild(listView);
        bottomContainer.addChild(myActivityIndicator);

        mainLayout.addChild(topContainer);
        mainLayout.addChild(followContainer);
        mainLayout.addChild(bottomContainer);
        this.layout.addChild(mainLayout);


        function addBoxText(text1,text2,container)
        {
            var textContainer1 = new AbsoluteLayout();
            textContainer1.width = 75;
            textContainer1.height = 45;
            
            var followTextLarge = new Label();
            componentStyler(".pgProfile.boxTextLarge")(followTextLarge);
            followTextLarge.text = text1;
            followTextLarge.font =  Font.create("Lato",24,Font.NORMAL);
            followTextLarge.backgroundColor = Color.TRANSPARENT;
            followTextLarge.textColor = Color.WHITE;
            
            var followTextSmall = new Label();
            componentStyler(".pgProfile.boxTextSmall")(followTextSmall);
            followTextSmall.text = text2;
            followTextSmall.font =  Font.create("Lato",10,Font.NORMAL);
            followTextSmall.backgroundColor = Color.TRANSPARENT;
            followTextSmall.textColor = Color.create("#50FFFFFF");
            
            textContainer1.addChild(followTextLarge);
            textContainer1.addChild(followTextSmall);
            container.addChild(textContainer1);
        }
        
        function addTrackText(text,container)
        {
            var label = new Label();
            label.flexGrow = 1;
            label.flexBasis = 1;
            label.textAlignment = TextAlignment.MIDCENTER;
            label.text = text;
            label.font =  Font.create("Lato",16,Font.NORMAL);
            label.backgroundColor = Color.TRANSPARENT;
            label.textColor = Color.BLACK;
            label.touchEnabled = false;
            container.addChild(label);
        }
        
        function addBoxDivider(container)
        {
            var divider = new AbsoluteLayout();
            divider.width = 1;
            divider.height = 50;
            divider.backgroundColor = Color.create('#20FFFFFF');
            container.addChild(divider);
        }
        
        function addSmallDivider(container)
        {
            var divider = new AbsoluteLayout();
            divider.width = 1;
            divider.alignSelf = FlexLayout.AlignSelf.STRETCH;
            divider.marginTop = 10;
            divider.marginBottom = 10;
            divider.backgroundColor = Color.create('#20000000');
            container.addChild(divider);
        }
        
        } catch(e) {
            Application.onUnhandledError(e);
        }
        
        
        function showAlertDialog(msg)
        {
            var myAlertView = new AlertView({
                title: "NativeFace",
                message: msg
            });
            myAlertView.addButton({
                index: AlertView.ButtonType.NEGATIVE, 
                text: "Cancel"
            });
            myAlertView.addButton({
                index: AlertView.ButtonType.POSITIVE, 
                text: "OK"
            });
            
            myAlertView.show();
        }
        
        
        
    }});
    
    
    
});    

module.exports = pgProfile;
