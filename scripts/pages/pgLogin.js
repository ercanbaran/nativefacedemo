/* globals */
const Page              = require("nf-core/ui/page");
const extend            = require("js-base/core/extend");
const componentStyler   = require("js-base/core/styler").componentStyler();
const FlexLayout        = require('nf-core/ui/flexlayout');
const AbsoluteLayout    = require('nf-core/ui/absolutelayout');
const Color             = require('nf-core/ui/color');
const ImageView         = require('nf-core/ui/imageview');
const Image             = require('nf-core/ui/image');
const Button            = require('nf-core/ui/button');
const Font              = require('nf-core/ui/font');
const Label             = require("nf-core/ui/label");
const TextBox           = require('nf-core/ui/textbox');
const TextAlignment     = require('nf-core/ui/textalignment')
const KeyboardType      = require('nf-core/ui/keyboardtype');
const System            = require('nf-core/device/system');
const Screen             = require('nf-core/device/screen');
const Animator          = require('nf-core/ui/animator')
const Timer             = require("nf-core/global/timer");
const Router            = require('nf-core/ui/router');
const PageConstants     = require('pages/PageConstants');

// const Data              = require("nf-core/data");


var pgLogin = extend(Page)(
    function(_super,params)
    {
        var self = this;
        var logoImage;
        var mainLayout;
        _super(this,{
    
    
    onShow : function(){
       
        
        //this.layout.applyLayout();
        Router.sliderDrawer.enabled = false;
        // if(Global.Pages)
        // {
        //   global.Pages.sliderDrawer.enabled = false

        // }
        
         
        	

        // var myTimer = Timer.setTimeout({
        // task: animate,
        // delay: 100 
        // });
        animate();
        	
        function animate()
        {
            Animator.animate(self.layout, 500, function() {
		                    logoImage.top = Number.NaN;
		                    logoImage.right = Number.NaN
		                    logoImage.left = 1;
		                    logoImage.bottom = 1;
		                    logoImage.width = 25;
		                    logoImage.height = 25;
		                }).then(500, function() {
		                	
		                    logoImage.top = Number.NaN;
		                    logoImage.left = Number.NaN;
		                    logoImage.right = 1;
		                    logoImage.bottom = 1;
		                    logoImage.width = 50;
		                    logoImage.height = 50;
		                }).then(500, function() {
		                    logoImage.top = Number.NaN;
		                    logoImage.left = Number.NaN;
		                    logoImage.right = 1;
		                    logoImage.bottom = 1;
		                    logoImage.width = 50;
		                    logoImage.height = 50;
		                    
		                }).then(500, function() {
		                	logoImage.left = Number.NaN;
		                	logoImage.bottom = Number.NaN;
		                    logoImage.top = 1;
		                    logoImage.right = 1;
		                    logoImage.width = 75;
		                    logoImage.height = 75;
		                }).then(500, function() {
		                	logoImage.bottom = Number.NaN;
		                    logoImage.right = Number.NaN;
		                    logoImage.top = 100;
		                    logoImage.left = Screen.width/2 - 50;
		                    
		                    logoImage.width = 100;
		                    logoImage.height = 100;
		                }).then(1000, function() {
		                	mainLayout.top = 0;
		                }).complete(function() {
		                    //alert("Animation complete.");
		                });
        }
        
        /*System.validateFingerPrint({
            onSuccess: function() {
                nextPage()
                //alert("valid finger");
            },
            onError: function() {
                //alert("invalid finger");
            }
        });*/
        
    },
    onLoad : function(){
         this.headerBar.visible = false;
        this.statusBar.visible = false;

        this.layout.backgroundColor = Color.createGradient({startColor: Color.create("#9D1B55"), endColor:Color.create("#D9595B"), direction: Color.GradientDirection.DIAGONAL_LEFT});
        
        mainLayout = new FlexLayout({
            flexGrow: 1,
            alignSelf : FlexLayout.AlignSelf.STRETCH,
            flexDirection: FlexLayout.FlexDirection.COLUMN,
            justifyContent : FlexLayout.JustifyContent.CENTER,
            alignItems : FlexLayout.AlignItems.CENTER,
            top : Screen.height,
            backgroundColor : Color.TRANSPARENT
        });
        
        
        
        var titleLabel = new Label();
        titleLabel.text = lang.appName
        titleLabel.font = Font.create("Lato",30,Font.NORMAL);
        titleLabel.backgroundColor = Color.TRANSPARENT;
        titleLabel.textColor = Color.WHITE;
        titleLabel.margin = 20
        titleLabel.textAlignment = TextAlignment.MIDCENTER;
        
        mainLayout.addChild(titleLabel);
        
        
        
        var usernameContainer = new FlexLayout();
        componentStyler(".pgLogin.inputContainer")(usernameContainer);
        usernameContainer.backgroundColor = Color.create("#50FFFFFF")
        usernameContainer.borderColor = Color.TRANSPARENT;

        mainLayout.addChild(usernameContainer);
        
        var inputUsername = new TextBox();
        componentStyler(".pgLogin.inputText")(inputUsername);
        inputUsername.hint =  lang['pgLogin.username'];
        inputUsername.ios.clearButtonEnabled  = true;

        // if(Data.containsVariable("username"))
        // {
        //     inputUsername.text = Data.getStringVariable("username");
        // }
        // inputUsername.onEditEnds =  function() {
        //     Data.setStringVariable("username", inputUsername.text);
        // }

         usernameContainer.addChild(inputUsername);
        
        
        var passwordContainer = new FlexLayout();
        componentStyler(".pgLogin.inputContainer")(passwordContainer);
        passwordContainer.backgroundColor = Color.create("#50FFFFFF")
        passwordContainer.borderColor = Color.TRANSPARENT;

        mainLayout.addChild(passwordContainer);
        
        var inputPassword = new TextBox();
        componentStyler(".pgLogin.inputText")(inputPassword);
        inputPassword.isPassword = true,
        inputPassword.hint = lang['pgLogin.password'];
        //inputPassword.clearButtonEnabled = true;
        inputPassword.keyboardType = KeyboardType.NUMBER;
        inputPassword.ios.clearButtonEnabled = true;
        passwordContainer.addChild(inputPassword);
        
        var buttonContainer = new FlexLayout({
            flexDirection: FlexLayout.FlexDirection.ROW,
            justifyContent : FlexLayout.JustifyContent.CENTER,
            alignItems : FlexLayout.AlignItems.CENTER,
            padding : 10,
            height : 60,
            marginTop:10
        });
        
        var btnSignIn = new Button({
            width : 200,
            height : 50,
            backgroundColor : Color.TRANSPARENT,
            borderWidth : 1 , 
            borderColor : Color.create('#30FFFFFF'),
            borderRadius : 10,
            font : Font.create("Lato",16,Font.NORMAL),
            textColor : Color.WHITE,
            text :  lang['pgLogin.signin'],
            onPress : nextPage
        });
        
        var labelOR = new Label();
        labelOR.text = lang['pgLogin.or'];
        labelOR.textColor = Color.create("#50FFFFFF");
        labelOR.font      = Font.create("Lato",16,Font.NORMAL),

        labelOR.marginLeft = 10;
        labelOR.backgroundColor = Color.TRANSPARENT;
        
        
        var btnFacebook = new ImageView();
        componentStyler(".pgLogin.iconSocial")(btnFacebook);
        btnFacebook.image = Image.createFromFile("images://Facebook.png");
        var btnTwitter = new ImageView();
        componentStyler(".pgLogin.iconSocial")(btnTwitter);
        btnTwitter.margin = 10;
        btnTwitter.image = Image.createFromFile("images://Twitter.png");
        buttonContainer.addChild(btnSignIn);
        buttonContainer.addChild(labelOR);
        buttonContainer.addChild(btnTwitter);
        buttonContainer.addChild(btnFacebook);
        
        mainLayout.addChild(buttonContainer);
        this.layout.addChild(mainLayout);
        this.layout.applyLayout();
        
        
        logoImage = new ImageView();
        	logoImage.image = Image.createFromFile("images://logo.png");
        	logoImage.width = 0;
        	logoImage.height = 0;
        	logoImage.imageFillType = ImageView.FillType.ASPECTFIT;
        	logoImage.positionType = FlexLayout.PositionType.ABSOLUTE;
        	this.layout.addChild(logoImage);
        

        function nextPage(){
            Router.go(PageConstants.PAGE_PROFILE)
        }
    }});
    
    
    
});    

module.exports = pgLogin;
