/* globals */
const Page              = require("nf-core/ui/page");
const extend            = require("js-base/core/extend");
const componentStyler   = require("js-base/core/styler").componentStyler();
const FlexLayout        = require('nf-core/ui/flexlayout');
const AbsoluteLayout    = require('nf-core/ui/absolutelayout');
const Color             = require('nf-core/ui/color');
const ImageView         = require('nf-core/ui/imageview');
const Image             = require('nf-core/ui/image');
const Button            = require('nf-core/ui/button');
const Font              = require('nf-core/ui/font');
const Label             = require("nf-core/ui/label");
const ListView          = require('nf-core/ui/listview');
const ListViewItem      = require('nf-core/ui/listviewitem');
const TextAlignment     = require('nf-core/ui/textalignment');
const HeaderBarItem     = require('nf-core/ui/headerbaritem');
const AlertView         = require('nf-core/ui/alertview');
const Slider            = require('nf-core/ui/slider');
const Switch            = require('nf-core/ui/switch');
const System            = require('nf-core/device/system');
const Hardware          = require('nf-core/device/hardware');
const StatusBarStyle    = require("nf-core/ui/statusbarstyle");
const DatePicker        = require('nf-core/ui/datepicker');
const QuickLook         = require("nf-core/ui/quicklook");
const VideoView         = require("nf-core/ui/videoview");
const WebView           = require("nf-core/ui/webview");
const Picker            = require("nf-core/ui/picker");
const Router            = require('nf-core/ui/router');
const PageConstants     = require('pages/PageConstants');

var pgRestaurantDetail = extend(Page)(
    function(_super,params)
    {
    	var self = this;
        _super(this, 
        {
		    onShow : function(){
		    	self.headerBar.title = lang['pgRestaurantDetail.title'];
		    	self.headerBar.visible = true;

            self.statusBar.ios.style = StatusBarStyle.LIGHTCONTENT;

		    },
		    onLoad : function(){
        self.headerBar.backgroundColor = Color.create("#9D1B55");
        self.headerBar.titleColor = Color.WHITE;
        self.headerBar.itemColor = Color.WHITE;
        self.headerBar.displayShowHomeEnabled = true;
        self.layout.backgroundColor = Color.createGradient({startColor: Color.create("#9D1B55"), endColor:Color.create("#D9595B"), direction: Color.GradientDirection.DIAGONAL_LEFT});

         self.layout.paddingTop = 50;
        //pgRestaurantDetail.layout.justifyContent = FlexLayout.JustifyContent.CENTER;
        self.alignItems = FlexLayout.AlignItems.CENTER;
        
        var addItem = new HeaderBarItem({
            image: Image.createFromFile("images://pin.png"),
            color : Color.WHITE,
            onPress: function() {
                 Router.go(PageConstants.PAGE_MAPVIEW)
            }
        });
        self.headerBar.setItems([addItem]);
        
        
        var imageView = new ImageView();
        imageView.image = Image.createFromFile("images://food.png");
        imageView.imageFillType = ImageView.FillType.ASPECTFIT;
        imageView.maxHeight = 100;
        imageView.marginBottom = 20;
        //imageView.marginTop = 100;
        self.layout.addChild(imageView);

        
        var webTitle = new Label();
        webTitle.text = "http://www.zomato.com";
        webTitle.font =  Font.create("Lato",12,Font.NORMAL);
        webTitle.backgroundColor = Color.TRANSPARENT;
        webTitle.textColor = Color.WHITE;
        webTitle.textAlignment = TextAlignment.TOPCENTER;
        webTitle.margin = 10;
        webTitle.alpha = 0.5;
        webTitle.onTouch = function()
        {
            Router.go(PageConstants.PAGE_WEBVIEW);
        }
        
        self.layout.addChild(webTitle);

        

        
        var container = new FlexLayout();
        container.flexDirection = FlexLayout.FlexDirection.COLUMN;
        container.alignItems = FlexLayout.AlignItems.STRETCH;
        container.backgroundColor = Color.WHITE;
        container.borderWidth = 1;
        container.borderColor = Color.TRANSPARENT;
        container.borderRadius = 10;
        container.margin = 10;
        
        
        var myDatePicker = new DatePicker();
        myDatePicker.onDateSelected = function(date) {
           // alert('Year: ' + date.getFullYear() + ' Month: ' + date.getMonth() + ' Day' + date.getDate());
            
            var month = date.getMonth();
            var monthText = month;
            if(month < 10)
            {
                monthText = "0" + month;
            }
            resultDate.text = date.getDate() + '.'  + monthText + '.' + date.getFullYear();
        };
        
        var subContainer1 = new FlexLayout();
        subContainer1.flexDirection = FlexLayout.FlexDirection.ROW;
        subContainer1.alignItems = FlexLayout.AlignItems.STRETCH;
        subContainer1.height = 60;
        subContainer1.enabled = true;
        
        
        var titleDate = new Label();
        titleDate.font =  Font.create("Lato",16,Font.NORMAL);
        titleDate.backgroundColor = Color.TRANSPARENT;
        titleDate.textColor = Color.BLACK;
        titleDate.textAlignment = TextAlignment.MIDLEFT;
        titleDate.text = lang['pgRestaurantDetail.date'];
        titleDate.margin = 10;
        titleDate.flexGrow = 1;
        subContainer1.addChild(titleDate);
        
        var resultDate = new Label();
        resultDate.font =  Font.create("Lato",16,Font.NORMAL);
        resultDate.backgroundColor = Color.TRANSPARENT;
        resultDate.textColor = Color.BLACK;
        resultDate.textAlignment = TextAlignment.TOPRIGHT;
        resultDate.alignSelf = FlexLayout.AlignSelf.CENTER;
        resultDate.text = "13.02.2015";
        resultDate.flexGrow = 1;
        resultDate.alpha = 0.5
        subContainer1.addChild(resultDate);
        
        resultDate.onTouch = function(){
            myDatePicker.show();
        }
        
        var calendarIcon = new ImageView({
            width: 30,
            height: 30,
            margin:10,
            imageFillType : ImageView.FillType.STRETCH,
            alignSelf : FlexLayout.AlignSelf.CENTER,
            image: Image.createFromFile("images://calendar.png"),
            touchEnabled :true
        });
        
        calendarIcon.onTouch = function(){
            myDatePicker.show();
        }
        
        
        
        subContainer1.addChild(calendarIcon);
        container.addChild(subContainer1);
        addDivider(container,0)
        
        var subContainer2 = new FlexLayout();
        subContainer2.flexDirection = FlexLayout.FlexDirection.ROW;
        subContainer2.alignItems = FlexLayout.AlignItems.STRETCH;
        subContainer2.height = 60;
        
        var titleGuest = new Label();
        titleGuest.font =  Font.create("Lato",16,Font.NORMAL);
        titleGuest.backgroundColor = Color.TRANSPARENT;
        titleGuest.textColor = Color.BLACK;
        titleGuest.textAlignment = TextAlignment.MIDLEFT;
        titleGuest.text = lang['pgRestaurantDetail.numberOfGuests'];
        titleGuest.margin = 10;
        titleGuest.flexGrow = 1;
        subContainer2.addChild(titleGuest);
        
        var resultGuest = new Label();
        resultGuest.font =  Font.create("Lato",16,Font.NORMAL);
        resultGuest.backgroundColor = Color.TRANSPARENT;
        resultGuest.textColor = Color.BLACK;
        resultGuest.textAlignment = TextAlignment.TOPRIGHT;
        resultGuest.alignSelf = FlexLayout.AlignSelf.CENTER;
        resultGuest.text = '5';
        resultGuest.flexGrow = 1;
        resultGuest.alpha = 0.5;
        resultGuest.touchEnabled = true;
        resultGuest.marginRight = 10;
        
        resultGuest.onTouch = function()
        {
            
            
            var items = [
                "1",
                "2",
                "3",
                "4",
                "5"
            ];
            var myPicker = new Picker({
                items: items,
                index: 4
            });
            
            myPicker.show(function(e){
                console.log(e.index)
                resultGuest.text = items[e.index];
            });

        }
        
        subContainer2.addChild(resultGuest);
        container.addChild(subContainer2);
        //addDivider(container,0)
    
        
        self.layout.addChild(container)
        
        var btnMenu = new Button({
            width : 200,
            height : 50,
            backgroundColor : Color.TRANSPARENT,
            borderWidth : 1 , 
            borderColor : Color.create('#30FFFFFF'),
            borderRadius : 10,
            font : Font.create("Lato",16,Font.NORMAL),
            textColor : Color.WHITE,
            text :  lang['pgRestaurantDetail.menu'],
            alignSelf : FlexLayout.AlignSelf.CENTER,
            marginTop : 20,
            onPress : function()
            {
                var ql = new QuickLook();
                var menuPDF = "assets://menu.pdf";
                ql.document = [menuPDF];
                ql.barColor = Color.create("#9D1B55");
                ql.itemColor = Color.WHITE;
                ql.statusBar.style = 1;
                ql.show(self);
            }
            
        });
        
        self.layout.addChild(btnMenu)
        
        var video = new VideoView();
        video.ios.page = self;
        
        video.loadURL("https://drive.google.com/uc?export=download&id=0B0ut7ADBffqCTG1Cb0tsdUdub28");
        video.setControllerEnabled(false);
        video.setLoopEnabled(true);
        
        video.flexGrow = 1;
        video.margin = 10;
        video.marginTop = 30;
        self.layout.paddingBottom = 10;
        self.layout.addChild(video);
        video.applyLayout();
        video.play();
    
        video.onReady = function(){
            console.log("Ready");
        };
        
        video.onFinish = function(){
            console.log("Finish");
        };
        
        // var l = new FlexLayout();
        // l.flexGrow =1;
        // l.backgroundColor = Color.create("#FF9900");
        // pgRestaurantDetail.layout.addChild(l)
        
        function addDivider(container,rightLeftMargin)
        {
            var divider = new AbsoluteLayout();
            divider.height = 1;
            divider.backgroundColor = Color.create('#20000000');
            divider.alignSelf = FlexLayout.AlignSelf.STRETCH;
            divider.marginTop = 5;
            divider.marginBottom = 5;
            if(rightLeftMargin)
            {
                divider.marginRight = rightLeftMargin;
                divider.marginLeft = rightLeftMargin;
            }
            container.addChild(divider);
        }
		    }
        });
	}
);  
module.exports = pgRestaurantDetail;
